import React from 'react';
import { Breadcrumb, BreadcrumbItem, Section, Container } from 'bloomer';
import {
    BrowserRouter as Router,
    Route,
    Link 
} from 'react-router-dom'

import './css/App.css';
//import logo from './logo.svg';

import CreatePage from './pages/CreatePage';
import GroupEditor from './pages/GroupEditor';

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = props;
    } 

    setGroup(group) {

        this.setState({
            sharing_group: group,
        });
    }

    render() {
        return (
            <Router>
                <Section>
                    <Container isFluid>
                        <Breadcrumb>
                            <ul>
                                <BreadcrumbItem><Link to="/create" >create</Link></BreadcrumbItem>
                                <BreadcrumbItem><Link to="/sharing-group" >edit sharing group</Link></BreadcrumbItem>
                            </ul>
                        </Breadcrumb>
                        <Route exact path="/create" render={(props) => 
                            <CreatePage onGroupCreated={this.setGroup.bind(this)} />
                        }/>
                        <Route exact path="/sharing-group" render={(props) => <GroupEditor group={this.state.sharing_group} />} />
                        
                    </Container>
                </Section>
            </Router>
        );
    }
}

export default App;