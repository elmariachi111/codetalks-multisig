import React from 'react';
import { Columns, Column, Box, Field, Label, Control, Button } from 'bloomer';
import AceEditor from 'react-ace';

import Parties from '../components/Parties';
import AddParty from '../components/AddParty';
import AddressBalance from '../components/AddressBalance';

import {iota} from '../components/iota';

import 'brace/mode/json';
import 'brace/theme/github';


class FinalizeButton extends React.Component  {

    finalize() {

        const digests = this.props.parties.map(p => p.digest);
        const address = new iota.multisig.address().absorb(digests).finalize();
       
        this.props.onFinalized(address);
    }

    render() {
        return (<Button isColor='warning' onClick={this.finalize.bind(this)}>Finalize</Button>);
    }
}



class GroupEditor extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            group: props.group,
            jsonContent: JSON.stringify(props.group || {}, null, '  ')
        }
        this.handleJsonChanged = this.handleJsonChanged.bind(this);
    }
    
    handleJsonChanged(val) {
        const newState = {jsonContent: val}
        try {
            const group = JSON.parse(val);
            newState.group = group;
        } catch(e) {}

        this.setState(newState);
    }

    onFinalized(address) {
        let group = Object.assign({}, this.state.group);
        group.wallet.address = address;
            
        this.setState({
            group,
            jsonContent: JSON.stringify(group || {}, null, '  ')
        })
    }

    onPartyAdded(party) {
        
        let group = Object.assign({}, this.state.group);
        group.wallet.parties.push(party);
            
        this.setState({
            group,
            jsonContent: JSON.stringify(group || {}, null, '  ')
        })
    }

    render() {
        const parties = this.state.group.wallet ? this.state.group.wallet.parties : [];
        const address = this.state.group.wallet ? this.state.group.wallet.address : null;

        return (
            <div>
                <Field>
                    <Label>Edit sharing group {this.state.group.name}</Label>
                    <Control>
                        <AceEditor
                            mode="json"
                            theme="github"
                            height="500px"
                            width="100%"
                            onChange={this.handleJsonChanged}
                            name="UNIQUE_ID_OF_DIV"
                            value={this.state.jsonContent}
                            editorProps={{$blockScrolling: true}}
                        />

                    </Control>
                </Field>

                <Parties parties={parties} />

                {!address ? <AddParty parties={parties} onPartyAdded={this.onPartyAdded.bind(this)}/> : null }
              

                {parties.length > 1 && !address ? <FinalizeButton onFinalized={this.onFinalized.bind(this)} parties={parties}/> : null}

                {iota.valid.isAddress(address) ? <AddressBalance address={address} /> : null}
                
            </div>
        );
    }
}

export default GroupEditor;
