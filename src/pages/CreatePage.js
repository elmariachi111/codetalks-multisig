import React from 'react';

import { Container  } from 'bloomer';
import CreateForm from '../components/CreateForm';
import {Redirect} from 'react-router-dom'

class CreatePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      submitted: false
    }
    this.handleCreated = this.handleCreated.bind(this); 
  }

  handleCreated(group) {
    this.props.onGroupCreated(group);
    this.setState({submitted: true});
  }

  render() {
    if (this.state.submitted) {
      return <Redirect to="/sharing-group" />;
    } else {
      return (
        <Container>
          <CreateForm submitted={this.state.submitted} onCreated={this.handleCreated}/>
        </Container>
      );
    }
  }
}

export default CreatePage;
