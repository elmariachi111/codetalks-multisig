import React from 'react';
import { Field, Label,  Input, Button, FieldBody } from 'bloomer';

import {iota} from '../components/iota';

const security = 2;

class AddButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    onAdd() {
        const index = 1;
        const digest = iota.multisig.getDigest(this.props.seed, index, security);
        const party = {
            id: this.props.name,
            security,
            indexMain: index,
            digest
        }
        this.setState({
            party,
            seed: this.props.seed
        });
        
        this.props.onPartyAdded(party);
    }

    render() {
        return (<Button isColor='primary' onClick={this.onAdd.bind(this)}>Add</Button>);
    }
}

export default class AddParty extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            seed: '',
            
        }
    }
    handleNameChange(event) {
        this.setState({name: event.target.value});
    }

    handleSeedChange(event) {
        this.setState({seed: event.target.value});
    }

    amIAlreadyAParty() {
        if (!this.state.group.wallet)
            return false;
        
        const inThere = this.state.group.wallet.parties.filter(p => p.id === this.state.name);
        return inThere.length > 0;

    }

    generateSeed() {
        const seedsize = 81;
        const chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ9";
        let seed = "";
        for (var i = 0, n = chars.length; i < seedsize; ++i) {
            seed += chars.charAt(Math.floor(Math.random() * n));
        }
        this.setState({ 
            seed: seed
        })
    }

    handlePartyAdded(party) {
        this.props.onPartyAdded(party);
        this.setState({
            party: party
        })
    }
    
    render() {
        if (this.state.party) {
            return (<Label>{this.state.name}, please note your seed: {this.state.seed}</Label>)
        } 
        
        return ( 
            <FieldBody>
                <Field >
                    <Input placeholder='Your name' value={this.state.name} onChange={this.handleNameChange.bind(this)} />
                </Field>
                <Field>
                    <Input placeholder='Your seed' value={this.state.seed} onChange={this.handleSeedChange.bind(this)} />
                </Field>
                <Field>
                    <Button isColor='primary' onClick={this.generateSeed.bind(this)}>Generate Seed</Button>
                </Field>
                <Field>
                    {this.state.name && this.state.seed ?
                    <AddButton {...this.state} onPartyAdded={this.handlePartyAdded.bind(this)}/>
                    : ''}
                </Field>
            </FieldBody>
        );
    }
}