import React from 'react';
import { Columns, Column, Box } from 'bloomer';

export default class Parties extends React.Component {
    render() {
        const boxes = this.props.parties.map( p => <Column key={p.id}><Box>{p.id}</Box></Column>);
        return (<Columns isCentered>{boxes}</Columns>);   
    }
}
