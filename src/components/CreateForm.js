import React from 'react';

import { Field, Label, Control, Input,  Button  } from 'bloomer';

class CreateForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
        }
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    handleNameChange(event) {
        this.setState({name: event.target.value});
    }

    handleSubmit(e) {
        e.preventDefault();
        const formData = new FormData(e.target);
        const group = {};
        formData.forEach((value, name) => {
            group[name] = value;
        });
        group.wallet = {
            parties: [],
            address: null
        };
        this.props.onCreated(group);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <Field>
                    <Label>We are sharing a {this.state.name}</Label>
                    <Control>
                        <Input type="text" name="name" placeholder="Container ship, cottage,..." value={this.state.name} onChange={this.handleNameChange} />
                    </Control>
                </Field>

                <Field>
                    <Label>To rent an item you must leave a deposit of</Label>
                    <Control>
                        <Input type="text" placeholder="1500i" name="deposit"  />
                    </Control>
                </Field>
                <Field>
                    <Label>Renting this item costs</Label>
                    <Control>
                        <Input type="text" placeholder="20i" name="fee" />
                        per day
                    </Control>
                </Field>
                <Field>
                    <Label>add yourself as first owner</Label>
                    <Control>
                        <Input type="text" placeholder="Your name" name="owner" />
                    </Control>
                </Field>
                <Button type="submit">
                    Start creating group for {this.state.name}
                </Button>
            </form>
        );
    }
}

export default CreateForm;
