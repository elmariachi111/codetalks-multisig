import React from 'react';
import { Label } from 'bloomer';
import {iota} from "./iota";

const treshold = 100; //probability for tx confirmation

class AddressBalance extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            address: props.address,
            balance: null
        }
    }
    
    getBalance() {
        iota.api.getBalances([this.state.address], treshold, (err, response) => {
            if (err) {
                console.error(err);
            }
            const balance = parseInt(response.balances[0],10);
            this.setState({balance: balance});
        });
        
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.address !== prevProps.address) {
            this.getBalance();
        }
    }

    componentDidMount() {
        this.getBalance();
    }

    render() {
        return (
            <Label>Address balance: {this.state.balance}</Label>            
        );
    }
}

export default AddressBalance;
